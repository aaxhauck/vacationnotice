<?php namespace Alex\Vacationnotice;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Alex\Vacationnotice\Components\Vacation' => 'vacation' 
        ];
    }

    public function registerSettings()
    {
        return [
            'config' => [
                'label'       => 'alex.vacationnotice::lang.plugin.name',
                'icon'        => 'icon-calendar',
                'category'    => 'Gunstore',
                'description' => 'Hier den Urlaub von bis eintragen',
                'class'       => 'Alex\Vacationnotice\Models\Settings',
                'permissions' => ['alex.vacationnotice.manage_plugin'],
                'order'       => 600
            ]
        ];
    }

    public function registerReportWidgets()
    {
        return [
            'Alex\Vacationnotice\ReportWidgets\VacInfo' => [
                'label'   => 'alex.vacationnotice::lang.plugin.name',
                'context' => 'dashboard'
            ]
        ];
    }

}
