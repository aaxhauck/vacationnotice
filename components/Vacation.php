<?php namespace Alex\Vacationnotice\Components;

use Cms\Classes\ComponentBase;
use Alex\Vacationnotice\Models\Settings;

class Vacation extends ComponentBase
{

	public function onRender()
	{
		$this->page['code_vacation_info_on'] 		= Settings::get('vacation_info_on');
		$this->page['code_vacation_txt_ab_start'] 	= Settings::get('vacation_date_txt_start');
		$this->page['code_vacation_info_start'] 	= Settings::get('vacation_date_start');
		$this->page['code_vacation_date_between'] 	= Settings::get('vacation_date_between');
		$this->page['code_vacation_info_end'] 		= Settings::get('vacation_date_end');
		$this->page['code_vacation_info_message'] 	= Settings::get('vacation_info_message');
	}

	public function componentDetails()
	{
		return [
			'name' => 'alex.vacationnotice::lang.plugin.name',
			'description' => 'alex.vacationnotice::lang.plugin.description',
		];
	}

	public function defineProperties()
	{
		return [
		];
	}

	public function onRun()
	{
	}

	protected function displayNotice()
	{
	}

	public $notice;
}