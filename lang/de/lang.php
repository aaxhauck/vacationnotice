<?php return [
    'plugin' => [
        'name' => 'Urlaubsinfo',
        'description' => 'Hier eine Urlaubs Info eintragen',
        'lbl_info_on' => 'Urlaubs Info anzeigen',
        'lbl_start_date' => 'Urlaub Beginn',
        'lbl_end_date' => 'Urlaub Ende',
        'lbl_info_text' => 'Info Text',
        'lbl_msg_before_start' => 'ab dem',
        'lbl_msg_between_dates' => 'Text zwischen Urlaub Start und Ende (bis, bis zum)',
    ],
];