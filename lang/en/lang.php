<?php return [
    'plugin' => [
        'name' => 'Vacationnotice',
        'description' => 'Insert your Vaction time here',
        'lbl_info_on' => 'Show Vaction info',
        'lbl_start_date' => 'Vacation start',
        'lbl_end_date' => 'Vacation end',
        'lbl_info_text' => 'Notice text',
		'lbl_msg_before_start' => 'starting from',
        'lbl_msg_between_dates' => 'Text in between start- and enddate',
    ],
];