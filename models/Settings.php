<?php namespace Alex\Vacationnotice\Models;

use October\Rain\Database\Model;

class Settings extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $implement = ['System.Behaviors.SettingsModel'];

    public $settingsCode = 'alex_vacationnotice_settings';

    public $settingsFields = 'fields.yaml';

    /**
     * Validation rules
     */
    public $rules = [
        'vacation_info_message' => ['required'],
        'vacation_date_start' => ['required']
    ];
}