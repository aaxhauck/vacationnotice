<?php namespace Alex\Vacationnotice\ReportWidgets;

use Backend\Classes\ReportWidgetBase;
use Alex\Vacationnotice\Models\Settings;

class VacInfo extends ReportWidgetBase
{


    public function render()
    {
        $this->loadData();  
        return $this->makePartial('widget');
    }

    public function defineProperties()
    {
        return [
            'title' => [
                'title'             => 'alex.vacationnotice::lang.plugin.name',
                'default'           => 'alex.vacationnotice::lang.plugin.name',
                'type'              => 'string',
                'validationMessage' => 'backend::lang.dashboard.widget_title_error'
            ]
        ];
    }

    private function loadData() {
        $this->vars['code_vacation_info_on']        = Settings::get('vacation_info_on');
        $this->vars['code_vacation_info_start']     = Settings::get('vacation_date_start');
        $this->vars['code_vacation_date_between']   = Settings::get('vacation_date_between');
        $this->vars['code_vacation_info_end']       = Settings::get('vacation_date_end');
        $this->vars['code_vacation_info_message']   = Settings::get('vacation_info_message');
    }

}
